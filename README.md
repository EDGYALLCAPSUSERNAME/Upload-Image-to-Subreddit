Upload Image To Subreddit
=========================

Installations
-------------

This script requires python 3, praw, and praw-oauth2util
To install praw type in your command line:

    pip install praw
    pip install praw-oauth2util

Reddit OAuth Setup
------------------

* [Go here on the account you want the bot to run on](https://www.reddit.com/prefs/apps/)
* Click on create a new app.
* Give it a name. 
* Select script from the radio buttons.
* Set the redirect uri to `http://127.0.0.1:65010/authorize_callback`
* After you create it you will be able to access the app key and secret.
* The **app key** is found here. Add this to the app_key variable in the oauth.txt file.

![here](http://i.imgur.com/7ybI5Fo.png) (**Do not give out to anybody**) 

* And the **app secret** here. Add this to the app_secret variable in the oauth.txt file.

![here](http://i.imgur.com/KkPE3EV.png) (**Do not give out to anybody**)

Note: On the first run of the bot you'll need to run it on a system that has a desktop environment.
So, if you're planning on running the bot on a server or VPS somewhere, run it on your computer
first. The first run will require you to authenticate it with the associated Reddit account by
opening a web page that looks like this:

![authentication page](http://i.imgur.com/se53uTq.png)

It will list different things it's allowed to do depending on the bots scope. After you
authenticate it, that page won't pop up again unless you change the OAuth credentials. And you'll
be free to run it on whatever environment you choose.

Config
------

Add the direct link to the images of the server status' to the IMAGE_URLS list. And
add your subreddit name to the SUBREDDIT_NAME variable (don't include the /r/).

Set the image name to the name you want the images (i.e. serverstatus). If you have multiple images set the variable APPEND_NUMBER_TO_IMAGE to True, otherwise
the previous image will get overwritten before it is uploaded. The image names
will then become 'serverstatus1', 'serverstatus2', etc.

License
-------

The MIT License (MIT)

Copyright (c) 2015 Nick Hurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

