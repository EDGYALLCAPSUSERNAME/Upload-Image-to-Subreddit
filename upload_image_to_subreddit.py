import os
import praw
import requests
import OAuth2Util

# User config
# -----------------------------------------------
IMAGE_URLS = []

SUBREDDIT_NAME = ''

# the name of the image to be uploaded
IMAGE_NAME = ''

# if you have multiple images set this to true
APPEND_NUMBER_TO_IMAGE = False
# -----------------------------------------------

def download_image(subreddit):
    # create the file
    print("Downloading image[s]...")
    for i in range(1, len(IMAGE_URLS) + 1):
        if APPEND_NUMBER_TO_IMAGE:
            img_name = IMAGE_NAME + str(i) + '.png'
        else:
            img_name = IMAGE_NAME + '.png'

        img = open(img_name, 'wb')
        # download the image and write it to file
        img.write(requests.get(IMAGE_URLS[i - 1]).content)
        # close the file
        img.close()

        # upload it and then delete it
        upload_image_to_sub(subreddit, img_name)
        delete_image(img_name)



def upload_image_to_sub(subreddit, img_name):
    # upload it to subreddit
    print("Uploading image[s] to subreddit...")
    subreddit.upload_image(img_name, name=img_name[:-4])


def delete_image(img_name):
    # delete the image
    print("Deleting image[s] from computer...")
    os.remove(img_name)  # remove the image from computer


def main():
    r = praw.Reddit('Upload server status image v2.0 /u/EDGYALLCAPSUSERNAME')
    OAuth2Util.OAuth2Util(r, print_log=True)  # Sign in
    subreddit = r.get_subreddit(SUBREDDIT_NAME)

    download_image(subreddit)


if __name__ == "__main__":
    main()
